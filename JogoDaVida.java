
public class JogoDaVida 
{
	private int[][]matrix;
	private int[][]matrixNova;
	public JogoDaVida(){
		int[][] matrix = new int[5][5];
		for(int i = 0; i< matrix.length ; i++) {
			for(int j = 0; j< matrix[i].length ; j++) {
				matrix[i][j] = 0;				
			}
		}
		matrix[3][2]= 1;
		matrix[3][3]= 1;
		matrix[3][4]= 1;

	}
	public static void main(String[] args) 
	{
		JogoDaVida jdv = new JogoDaVida();
		jdv.executar();
	}

	public void executar(){
		for (int i = 0; i < matrix.length; i++){
			for (int j = 0; j < matrix[i].length; j++){
				int numVivos = contarVivos(i,j,matrix);
				// Analisando vivos
				if(matrix[i][j] == 1) 
				{
					if (numVivos < 2) {
						matrixNova[i][j] = 0;
					}
					else if (numVivos > 3) {
						matrixNova[i][j] = 0;
					}
				}
				//Analisando mortos
				if(matrix[i][j] == 0) 
				{
					if(numVivos==3){
						matrixNova[i][j] = 0;	
					}
				}

			}
		}
	}

	public int contarVivos(int x, int y, int matrix[][])
	{
		int linha = x;
		int coluna = y;
		int numVivos = 0;

		if(matrix[linha-1][coluna] == 1)
			numVivos++;
		if(matrix[linha+1][coluna] == 1)
			numVivos++;
		if(matrix[linha][coluna +1] == 1)
			numVivos++;
		if(matrix[linha][coluna -1] == 1)
			numVivos++;
		if(matrix[linha-1][coluna -1] == 1)
			numVivos++;
		if(matrix[linha-1][coluna +1] == 1)
			numVivos++;
		if(matrix[linha+1][coluna -1] == 1)
			numVivos++;
		if(matrix[linha+1][coluna+1] == 1)
			numVivos++;
		return numVivos;
	}
}
